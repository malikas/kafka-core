package com.pcb.kafkacore.common.serde;

import java.util.Map;

public interface Deserializer<T> {

    default void configure(Map<String, ?> configs, boolean isKey) {
    }

    T deserialize(String var1, byte[] var2);

    default void close() {
    }

}
