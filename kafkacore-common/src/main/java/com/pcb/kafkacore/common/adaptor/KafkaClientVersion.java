package com.pcb.kafkacore.common.adaptor;

public enum KafkaClientVersion {

    V26("org.apache.kafka26"), // Kafka 2.6.x
    UNKNOWN("org.apache.kafka");

    private final String shadedPackageName;

    KafkaClientVersion(String shadedPackageName) {
        this.shadedPackageName = shadedPackageName;
    }

    public String getShadedPackageName() {
        return shadedPackageName;
    }
}
