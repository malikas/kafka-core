package com.pcb.kafkacore.consumer.service.impl;

import com.pcb.kafkacore.consumer.adaptor.NativeConsumerAdaptor;
import com.pcb.kafkacore.consumer.factory.NativeConsumerAdaptorFactory;
import com.pcb.kafkacore.consumer.model.ConsumerRebalanceListener;
import com.pcb.kafkacore.consumer.model.ConsumerRecords;
import com.pcb.kafkacore.common.serde.Deserializer;
import com.pcb.kafkacore.consumer.model.OffsetResetStrategy;
import com.pcb.kafkacore.consumer.model.TopicPartition;
import com.pcb.kafkacore.consumer.service.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KafkaConsumerImpl<K, V> implements KafkaConsumer<K, V> {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumerImpl.class);

    @Nonnull private final String consumerName;
    @Nonnull private final NativeConsumerAdaptor<K, V> consumer;
    @Nonnull private final OffsetResetStrategy offsetResetStrategy;

    /**
     * Constructor.
     *
     * @param consumerName    Name used by this consumer for identification in logging and metrics
     * @param kafkaProperties Properties to use for initializing the underlying Kafka client
     */
    public KafkaConsumerImpl(@Nonnull String consumerName, @Nonnull Properties kafkaProperties) {
        this(consumerName, kafkaProperties, null, null);
    }

    /**
     * Constructor.
     *
     * @param consumerName      Name used by this consumer for identification in logging and metrics
     * @param kafkaProperties   Properties to use for initializing the underlying Kafka client
     * @param keyDeserializer   Used for deserializing message keys; if null, it should be defined in kafkaProperties
     * @param valueDeserializer Used for deserializing message values; if null, it should be defined in kafkaProperties
     */
    public KafkaConsumerImpl(@Nonnull String consumerName, @Nonnull Properties kafkaProperties, @Nullable Deserializer<K> keyDeserializer, @Nullable Deserializer<V> valueDeserializer) {
        this.consumerName = Objects.requireNonNull(consumerName);
        this.consumer = NativeConsumerAdaptorFactory.create(this.consumerName, Objects.requireNonNull(kafkaProperties), keyDeserializer, valueDeserializer);

        OffsetResetStrategy defaultOffsetResetStrategy = OffsetResetStrategy.LATEST; // This is also the default value used by the native client
        String offsetResetPolicyStr = kafkaProperties.getProperty("auto.offset.reset", defaultOffsetResetStrategy.name());
        this.offsetResetStrategy = Stream.of(OffsetResetStrategy.values())
                .filter(offsetResetPolicy -> offsetResetPolicy.name().equalsIgnoreCase(offsetResetPolicyStr))
                .findAny()
                .orElse(defaultOffsetResetStrategy);
    }

    @Override
    public void shutdown() {
        logger.info("{}: Unsubscribing from all topics and shutting down", consumerName);
        try {
            consumer.unsubscribe();
            consumer.close();
        } catch (RuntimeException e) {
            logger.error("{}: Failed to gracefully shutdown Kafka consumer", consumerName, e);
        }
    }

    @Override
    public void subscribe(@Nonnull List<String> topics, @Nonnull ConsumerRebalanceListener rebalanceListener) throws RuntimeException {
        Objects.requireNonNull(topics);
        Objects.requireNonNull(rebalanceListener);
        logger.info("{}: Subscribing to topics: {}", consumerName, topics);
        consumer.subscribe(topics, rebalanceListener);
    }

    @Override
    public void assign(@Nonnull String topic, @Nonnull Collection<Integer> partitions) throws RuntimeException {
        Objects.requireNonNull(topic);
        Objects.requireNonNull(partitions);
        assign(toTopicPartitions(topic, partitions));
    }

    @Override
    public void assign(@Nonnull Collection<TopicPartition> partitions) {
        Objects.requireNonNull(partitions);
        logger.info("{}: Assigning partitions: {}", consumerName, partitions);
        consumer.assign(partitions);
    }

    @Override
    public void seek(@Nonnull Map<TopicPartition, Long> requestedOffsets) throws RuntimeException {
        logger.debug("{}: Seeking to offsets: {}", consumerName, requestedOffsets);
        requestedOffsets.forEach(((topicPartition, offset) -> {
            try {
                consumer.seek(topicPartition, offset);
            } catch (RuntimeException e) {
                logger.error("{}: Failed to seek to offset {} for topic partition {}: {}", consumerName, offset, topicPartition, e.getMessage());
                throw e;
            }
        }));
    }

    @Nonnull
    @Override
    public ConsumerRecords<K, V> poll(long timeoutInMs) throws RuntimeException {
        logger.debug("{}: Polling for records [timeoutInMs={}]", consumerName, timeoutInMs);
        return consumer.poll(timeoutInMs);
    }

    @Nonnull
    @Override
    public ConsumerRecords<K, V> immediatePoll() throws RuntimeException {
        return poll(0);
    }

    @Override
    public void commit() {
        logger.trace("{}: Committing current offset positions", consumerName);
        consumer.commitSync();
    }

    @Override
    public void commit(@Nonnull String topic, @Nonnull Map<Integer, Long> offsetsByPartition) {
        Objects.requireNonNull(topic);
        Objects.requireNonNull(offsetsByPartition);
        commit(toTopicPartitions(topic, offsetsByPartition));
    }

    @Override
    public void commit(@Nonnull Map<TopicPartition, Long> offsetsByPartition) {
        Objects.requireNonNull(offsetsByPartition);
        logger.trace("{}: Committing offsets: {}", consumerName, offsetsByPartition);
        consumer.commitSync(offsetsByPartition);
    }

    @Nonnull
    @Override
    public Map<TopicPartition, Long> getLastCommittedOffsetsForAssignedPartitions() {
        Set<TopicPartition> assignment = consumer.assignment();
        return getLastCommittedOffsets(assignment);
    }

    @Nonnull
    @Override
    public Map<Integer, Long> getLastCommittedOffsetsForAssignedPartitions(@Nonnull String topic) {
        Objects.requireNonNull(topic);
        Map<TopicPartition, Long> lastCommittedOffsets = getLastCommittedOffsetsForAssignedPartitions();
        return filterOffsetsForTopic(topic, lastCommittedOffsets);
    }

    @Nonnull
    @Override
    public Map<Integer, Long> getLastCommittedOffsets(@Nonnull String topic) {
        Objects.requireNonNull(topic);
        Set<Integer> partitions = getPartitionsForTopic(topic);
        Set<TopicPartition> topicPartitions = toTopicPartitions(topic, partitions);
        Map<TopicPartition, Long> offsetsByPartition = getLastCommittedOffsets(topicPartitions);
        return filterOffsetsForTopic(topic, offsetsByPartition);
    }

    @Nonnull
    @Override
    public Map<Integer, Long> getBeginningOffsets(@Nonnull String topic) {
        Objects.requireNonNull(topic);
        logger.trace("{}: Retrieving the earliest offsets for topic: {}", consumerName, topic);
        Set<Integer> partitions = getPartitionsForTopic(topic);
        Set<TopicPartition> topicPartitions = toTopicPartitions(topic, partitions);
        Map<TopicPartition, Long> offsetsByPartition = consumer.beginningOffsets(topicPartitions);
        return filterOffsetsForTopic(topic, offsetsByPartition);
    }

    @Nonnull
    @Override
    public synchronized Map<Integer, Long> getEndOffsets(@Nonnull String topic) {
        Objects.requireNonNull(topic);
        logger.trace("{}: Retrieving the latest offsets for topic: {}", consumerName, topic);
        Set<Integer> partitions = getPartitionsForTopic(topic);
        Set<TopicPartition> topicPartitions = toTopicPartitions(topic, partitions);
        Map<TopicPartition, Long> offsetsByPartition = consumer.endOffsets(topicPartitions);
        return filterOffsetsForTopic(topic, offsetsByPartition);
    }

    @Nonnull
    @Override
    public synchronized Set<Integer> getPartitionsForTopic(@Nonnull String topic) {
        Objects.requireNonNull(topic);
        List<Integer> partitions = consumer.partitionsFor(topic);
        return new HashSet<>(partitions);
    }

    @Nonnull
    private static Set<TopicPartition> toTopicPartitions(@Nonnull String topic, @Nonnull Collection<Integer> partitions) {
        return partitions.stream()
                .map(partition -> new TopicPartition(topic, partition))
                .collect(Collectors.toSet());
    }

    @Nonnull
    private static Map<TopicPartition, Long> toTopicPartitions(@Nonnull String topic, @Nonnull Map<Integer, Long> offsetsByPartition) {
        return offsetsByPartition.entrySet().stream()
                .collect(Collectors.toMap(e -> new TopicPartition(topic, e.getKey()), Map.Entry::getValue));
    }

    @Nonnull
    private static Map<Integer, Long> filterOffsetsForTopic(@Nonnull String topic, @Nonnull Map<TopicPartition, Long> offsetsByPartition) {
        return offsetsByPartition.entrySet().stream()
                .filter(e -> topic.equals(e.getKey().topic()))
                .collect(Collectors.toMap(e -> e.getKey().partition(), Map.Entry::getValue));
    }

    @Nonnull
    private Map<TopicPartition, Long> getLastCommittedOffsets(Set<TopicPartition> partitions) {
        logger.trace("{}: Retrieving the last committed offsets partitions: {}", consumerName, partitions);

        try {
            Map<TopicPartition, Long> earliestOffsets = consumer.beginningOffsets(partitions);
            Map<TopicPartition, Long> committedOffsets = consumer.committed(partitions);
            Map<TopicPartition, Long> latestOffsets = consumer.endOffsets(partitions);

            /*
             * The committed offset refers to the next message to be retrieved in a poll() request (i.e., the next
             * unprocessed message).
             */
            for (TopicPartition topicPartition : partitions) {
                Long earliestOffset = earliestOffsets.get(topicPartition);
                Long committedOffset = committedOffsets.get(topicPartition);
                Long latestOffset = latestOffsets.get(topicPartition);

                if (committedOffset == null || committedOffset < earliestOffset || committedOffset > latestOffset + 1) {
                    switch (offsetResetStrategy) {
                        case NONE:
                            if (committedOffset == null) {
                                throw new RuntimeException("No offsets exist for partition " + topicPartition + " and reset policy is set to NONE");
                            } else {
                                throw new RuntimeException("Committed offset " + committedOffset + " is outside of offset range [" + earliestOffset + ", " + (latestOffset + 1) + "] and reset policy is set to NONE");
                            }
                        case EARLIEST:
                            committedOffsets.put(topicPartition, earliestOffset);
                            break;
                        case LATEST:
                            committedOffsets.put(topicPartition, latestOffset + 1);
                            break;
                    }
                }
            }

            return committedOffsets;
        } catch (RuntimeException e) {
            logger.error("{}: Error retrieving last committed offset for topics/partitions {}: {}", consumerName, partitions, e.getMessage());
            throw e;
        }
    }
}
