package com.pcb.kafkacore.consumer.factory;

import com.pcb.kafkacore.common.adaptor.KafkaClientVersion;
import com.pcb.kafkacore.common.util.EnumHelper;
import com.pcb.kafkacore.consumer.adaptor.NativeConsumerAdaptor;
import com.pcb.kafkacore.consumer.adaptor.v26.Kafka26ConsumerAdaptor;
import com.pcb.kafkacore.common.serde.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Properties;

public class NativeConsumerAdaptorFactory {

    private static final Logger logger = LoggerFactory.getLogger(NativeConsumerAdaptorFactory.class);
    private static final String NATIVE_CLIENT_VERSION_CONFIG_KEY = "kafkacore.nativeClientVersion";

    private NativeConsumerAdaptorFactory() {
    }

    @Nonnull
    public static <K, V> NativeConsumerAdaptor<K, V> create(@Nonnull String consumerName, @Nonnull Properties properties, @Nullable Deserializer<K> keyDeserializer, @Nullable Deserializer<V> valueDeserializer) {
        Properties nativeClientProperties = (Properties) properties.clone();
        String nativeClientVersionStr = (String) nativeClientProperties.remove(NATIVE_CLIENT_VERSION_CONFIG_KEY);
        KafkaClientVersion nativeClientVersion = EnumHelper.fromString(KafkaClientVersion.class, nativeClientVersionStr, KafkaClientVersion.UNKNOWN);

        switch (nativeClientVersion) {
            case V26:
                logger.info("{}: Using Kafka client version 2.6.x", consumerName);
                org.apache.kafka.common.serialization.Deserializer<K> kafka26KeyDeserializer = NativeDeserializerFactory.createKafka26Deserializer(keyDeserializer);
                org.apache.kafka.common.serialization.Deserializer<V> kafka26ValueDeserializer = NativeDeserializerFactory.createKafka26Deserializer(valueDeserializer);
                org.apache.kafka.clients.consumer.KafkaConsumer<K, V> underlyingKafka26Consumer = NativeConsumerFactory.newKafka26Consumer(nativeClientProperties, kafka26KeyDeserializer, kafka26ValueDeserializer);
                return new Kafka26ConsumerAdaptor<>(underlyingKafka26Consumer);
            default:
                throw new IllegalArgumentException("Got unsupported KafkaPublisher type for consumer " + consumerName + ": " + nativeClientVersionStr);
        }
    }
}
