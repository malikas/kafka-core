package com.pcb.kafkacore.consumer.factory;

import com.pcb.kafkacore.common.adaptor.KafkaClientVersion;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Properties;
import java.util.function.Function;

public class NativeConsumerFactory {
    private NativeConsumerFactory() {
    }

    /**
     * Creates a new Kafka 2.6.x consumer using the given set of properties.
     *
     * @param properties        The properties used by the newly constructed consumer
     * @param keyDeserializer   Used for deserializing message keys; if set to null, key.deserializer must be defined in properties
     * @param valueDeserializer Used for deserializing message values; if set to null, value.deserializer must be defined in properties
     * @return The new Kafka 2.6.x consumer
     * @throws RuntimeException If an exception occurred while trying to create the consumer (e.g. due to misconfiguration)
     */
    @Nonnull
    public static <K, V> org.apache.kafka.clients.consumer.KafkaConsumer<K, V> newKafka26Consumer(@Nonnull Properties properties,
                                                                                                    @Nullable org.apache.kafka.common.serialization.Deserializer<K> keyDeserializer,
                                                                                                    @Nullable org.apache.kafka.common.serialization.Deserializer<V> valueDeserializer) {
        Function<Properties, org.apache.kafka.clients.consumer.KafkaConsumer<K, V>> consumerConstructor = effectiveProperties
                -> new org.apache.kafka.clients.consumer.KafkaConsumer<>(effectiveProperties, keyDeserializer, valueDeserializer);
        return newKafkaConsumer(properties, KafkaClientVersion.V26, consumerConstructor);
    }

    private static <CONSUMER> CONSUMER newKafkaConsumer(Properties properties, KafkaClientVersion version, Function<Properties, CONSUMER> consumerConstructor) {
        return consumerConstructor.apply(properties);
    }
}
