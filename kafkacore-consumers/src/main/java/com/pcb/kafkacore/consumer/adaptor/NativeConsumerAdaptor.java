package com.pcb.kafkacore.consumer.adaptor;

import com.pcb.kafkacore.consumer.model.ConsumerRebalanceListener;
import com.pcb.kafkacore.consumer.model.ConsumerRecords;
import com.pcb.kafkacore.consumer.model.TopicPartition;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface NativeConsumerAdaptor<K, V> {

    void close();

    void subscribe(Collection<String> topics, ConsumerRebalanceListener listener);

    void assign(Collection<TopicPartition> partitions);

    void unsubscribe();

    Set<TopicPartition> assignment();

    List<Integer> partitionsFor(String topic);

    Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions);

    Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions);

    Map<TopicPartition, Long> committed(Set<TopicPartition> partitions);

    void seek(TopicPartition partition, long offset);

    ConsumerRecords<K, V> poll(long timeoutInMs);

    void commitSync();

    void commitSync(Map<TopicPartition, Long> offsets);

}
