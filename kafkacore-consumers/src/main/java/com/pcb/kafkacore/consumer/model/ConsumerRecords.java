package com.pcb.kafkacore.consumer.model;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Interface of the ConsumerRecords class used by the underlying KafkaConsumer API.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface ConsumerRecords<K, V> extends Iterable<ConsumerRecord<K, V>> {

    List<ConsumerRecord<K, V>> records(TopicPartition topicPartition);

    Iterable<ConsumerRecord<K, V>> records(String topic);

    Set<TopicPartition> partitions();

    Iterator<ConsumerRecord<K, V>> iterator();

    int count();

    boolean isEmpty();

}
