package com.pcb.kafkacore.consumer.factory;

import com.pcb.kafkacore.common.serde.Deserializer;

import javax.annotation.Nullable;
import java.util.Map;

public class NativeDeserializerFactory {
    private NativeDeserializerFactory() {
    }

    @Nullable
    public static <T> org.apache.kafka.common.serialization.Deserializer<T> createKafka26Deserializer(@Nullable Deserializer<T> deserializer) {
        if (deserializer == null) {
            return null;
        }

        return new org.apache.kafka.common.serialization.Deserializer<>() {

            @Override
            public void configure(Map<String, ?> configs, boolean isKey) {
                deserializer.configure(configs, isKey);
            }

            @Override
            public T deserialize(String s, byte[] bytes) {
                return deserializer.deserialize(s, bytes);
            }

            @Override
            public void close() {
                deserializer.close();
            }

        };
    }
}
