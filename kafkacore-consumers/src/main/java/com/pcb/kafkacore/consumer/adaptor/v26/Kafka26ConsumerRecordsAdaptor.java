package com.pcb.kafkacore.consumer.adaptor.v26;

import com.google.common.collect.Iterators;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Adaptor for the Kafka 2.6 ConsumerRecords class
 * @param <K> Key type
 * @param <V> Value type
 */
public class Kafka26ConsumerRecordsAdaptor<K, V> implements com.pcb.kafkacore.consumer.model.ConsumerRecords<K, V> {

    private final ConsumerRecords<K, V> nativeRecords;

    private Kafka26ConsumerRecordsAdaptor(ConsumerRecords<K, V> nativeRecords) {
        this.nativeRecords = Objects.requireNonNull(nativeRecords);
    }

    public static <K, V> Kafka26ConsumerRecordsAdaptor<K, V> from(ConsumerRecords<K, V> nativeRecords) {
        return nativeRecords == null ? null : new Kafka26ConsumerRecordsAdaptor<>(nativeRecords);
    }

    @Override
    public List<com.pcb.kafkacore.consumer.model.ConsumerRecord<K, V>> records(com.pcb.kafkacore.consumer.model.TopicPartition topicPartition) {
        TopicPartition nativeTopicPartition = topicPartition == null ? null : new TopicPartition(topicPartition.topic(), topicPartition.partition());
        List<ConsumerRecord<K, V>> nativeResult = nativeRecords.records(nativeTopicPartition);
        return nativeResult == null ? null : nativeResult.stream().map(Kafka26ConsumerRecordAdaptor::from).collect(Collectors.toList());
    }

    @Override
    public Iterable<com.pcb.kafkacore.consumer.model.ConsumerRecord<K, V>> records(String topic) {
        Iterable<ConsumerRecord<K, V>> nativeResult = nativeRecords.records(topic);
        return nativeResult == null ? null : StreamSupport.stream(nativeResult.spliterator(), false)
                .map(Kafka26ConsumerRecordAdaptor::from)
                .collect(Collectors.toList());
    }

    @Override
    public Set<com.pcb.kafkacore.consumer.model.TopicPartition> partitions() {
        Set<TopicPartition> nativeTopicPartitions = nativeRecords.partitions();
        return nativeTopicPartitions == null ? null : nativeTopicPartitions.stream()
                .map(nativeTopicPartition -> new com.pcb.kafkacore.consumer.model.TopicPartition(nativeTopicPartition.topic(), nativeTopicPartition.partition()))
                .collect(Collectors.toSet());
    }

    @Override
    public Iterator<com.pcb.kafkacore.consumer.model.ConsumerRecord<K, V>> iterator() {
        Iterator<ConsumerRecord<K, V>> nativeIterator = nativeRecords.iterator();
        return nativeIterator == null ? null : Iterators.transform(nativeIterator, Kafka26ConsumerRecordAdaptor::from);
    }

    @Override
    public int count() {
        return nativeRecords.count();
    }

    @Override
    public boolean isEmpty() {
        return nativeRecords.isEmpty();
    }
}
