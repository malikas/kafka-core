package com.pcb.kafkacore.consumer.model;

import java.util.Collection;

public class NoOpConsumerRebalanceListener implements ConsumerRebalanceListener {
    @Override
    public void onPartitionsRevoked(Collection<TopicPartition> revokedTopicTartitions) {

    }

    @Override
    public void onPartitionsAssigned(Collection<TopicPartition> assignedTopicPartitions) {

    }
}
