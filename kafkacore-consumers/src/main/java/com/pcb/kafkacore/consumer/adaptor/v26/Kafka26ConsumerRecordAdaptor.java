package com.pcb.kafkacore.consumer.adaptor.v26;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Objects;

/**
 * Adaptor for the Kafka 2.6 ConsumerRecord class.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class Kafka26ConsumerRecordAdaptor<K, V> implements com.pcb.kafkacore.consumer.model.ConsumerRecord<K, V> {

    private final ConsumerRecord<K, V> nativeRecord;

    private Kafka26ConsumerRecordAdaptor(ConsumerRecord<K, V> nativeRecord) {
        this.nativeRecord = Objects.requireNonNull(nativeRecord);
    }

    public static <K, V> Kafka26ConsumerRecordAdaptor<K, V> from(ConsumerRecord<K, V> nativeRecord) {
        return nativeRecord == null ? null : new Kafka26ConsumerRecordAdaptor<>(nativeRecord);
    }

    @Override
    public String topic() {
        return nativeRecord.topic();
    }

    @Override
    public int partition() {
        return nativeRecord.partition();
    }

    @Override
    public K key() {
        return nativeRecord.key();
    }

    @Override
    public V value() {
        return nativeRecord.value();
    }

    @Override
    public long offset() {
        return nativeRecord.offset();
    }

    @Override
    public long timestamp() {
        return nativeRecord.timestamp();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kafka26ConsumerRecordAdaptor<?, ?> that = (Kafka26ConsumerRecordAdaptor<?, ?>) o;
        return Objects.equals(nativeRecord, that.nativeRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nativeRecord);
    }
}
