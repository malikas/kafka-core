package com.pcb.kafkacore.consumer.model;

/**
 * Defines the strategy to use when polling offets/messages for non-existant or out-of-range offsets. These correspond
 * to the reset policies defined in the underlying Kafka client.
 */
public enum OffsetResetStrategy {

    /**
     * Automatically reset the offset to the latest available offset.
     */
    LATEST,

    /**
     * Automatically reset the offset to the earliest available offset.
     */
    EARLIEST,

    /**
     * Throw exception to the consumer if no previous offset is found for the consumer's group.
     */
    NONE;

}
