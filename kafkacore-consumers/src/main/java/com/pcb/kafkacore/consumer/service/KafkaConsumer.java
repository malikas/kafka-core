package com.pcb.kafkacore.consumer.service;

import com.pcb.kafkacore.consumer.model.ConsumerRebalanceListener;
import com.pcb.kafkacore.consumer.model.ConsumerRecords;
import com.pcb.kafkacore.consumer.model.TopicPartition;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provides high-level Kafka client operations
 *
 * @param <K> Kafka message key type
 * @param <V> Kafka message value type
 */
public interface KafkaConsumer<K, V> {

    /**
     * Subscribe to the given topics.
     *
     * @param topics List of topics to subscribe to
     * @param rebalanceListener Listener for getting/processing topic partition assignment notifications
     * @throws RuntimeException If an error occurs while invoking the underlying Kafka consumer
     */
    void subscribe(@Nonnull List<String> topics, @Nonnull ConsumerRebalanceListener rebalanceListener) throws RuntimeException;

    /**
     * Manually assigns the requested partitions to this consumer.
     *
     * This method cannot be used in conjunction with {@link #subscribe(List, ConsumerRebalanceListener)} which uses
     * automatic partition assignment instead of manual partition assignment.
     *
     * @param topic      Topic whose partitions to
     * @param partitions Collection of partitions to assign to this consumer
     * @throws RuntimeException If an error occurs while invoking the underlying Kafka consumer
     */
    void assign(@Nonnull String topic, @Nonnull Collection<Integer> partitions) throws RuntimeException;

    /**
     * Manually assigns the requested partitions to this consumer.
     *
     * This method cannot be used in conjunction with {@link #subscribe(List, ConsumerRebalanceListener)} which uses
     * automatic partition assignment instead of manual partition assignment.
     *
     * @param partitions Collection of partitions to assign to this consumer
     * @throws RuntimeException If an error occurs while invoking the underlying Kafka consumer
     */
    void assign(@Nonnull Collection<TopicPartition> partitions) throws RuntimeException;

    /**
     * Seeks the underlying consumer to the provided offsets (i.e. the next call to {@link #poll(long)} will return the
     * messages at these offsets).
     *
     * @param offsetsByPartition Offsets to seek to
     * @throws RuntimeException If the consumer failed (partially or completely) to seek to the desired offsets
     */
    void seek(@Nonnull Map<TopicPartition, Long> offsetsByPartition) throws RuntimeException;

    /**
     * Retrieves messages at the current offsets, blocking for timeoutInMs milliseconds if no data is available. If no
     * new records arrive in that time, this method returns an empty set of records.
     *
     * @param timeoutInMs Maximum number of milliseconds to wait for new records
     * @return The retrieved records
     * @throws RuntimeException If an error occurs while invoking the underlying Kafka consumer
     */
    @Nonnull
    ConsumerRecords<K, V> poll(long timeoutInMs) throws RuntimeException;

    /**
     * Retrieves messages at the current offsets. If no new records are available, this method returns immediately with
     * an empty set of records.
     *
     * @return The retrieved records
     * @throws RuntimeException If an error occurs while invoking the underlying Kafka consumer
     */
    @Nonnull
    ConsumerRecords<K, V> immediatePoll() throws RuntimeException;

    /**
     * Commits the current offset positions to Kafka. These offsets are the offsets of the next messages to consume,
     * i.e. the last processed offsets + 1.
     *
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    void commit() throws RuntimeException;

    /**
     * Commits the given offsets for a particular topic to Kafka. These offsets should be the offsets of the next
     * messages to consume, i.e. the last processed offsets + 1.
     *
     * The offsets committed via this method must correspond to partitions that are assigned to this consumer.
     *
     * @param topic              Topic for which to commit offsets
     * @param offsetsByPartition Offsets to commit, indexed by partition; all keys/values must not be null
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    void commit(@Nonnull String topic, @Nonnull Map<Integer, Long> offsetsByPartition) throws RuntimeException;

    /**
     * Commits the given offsets to Kafka. These offsets should be the offsets of the next messages to consume, i.e.
     * the last processed offsets + 1.
     *
     * The offsets committed via this method must correspond to partitions that are assigned to this consumer.
     *
     * @param offsetsByPartition Offsets to commit, indexed by topic/partition; all keys/values must not be null
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    void commit(@Nonnull Map<TopicPartition, Long> offsetsByPartition) throws RuntimeException;

    /**
     * Returns the last committed offsets for all assigned partitions across all subscribed topics. These offsets
     * correspond to the consumer position, i.e. the offsets of the next messages that should be polled/processed.
     *
     * If the last committed offsets are outside the range of valid offsets, or if offsets haven't yet been committed,
     * then the returns offset will be appropriately set/adjusted based on the 'auto.offset.reset' property defined
     * in the underlying client properties.
     *
     * @return The last committed offsets for all assigned partitions across all subscribed topics
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Map<TopicPartition, Long> getLastCommittedOffsetsForAssignedPartitions() throws RuntimeException;

    /**
     * Returns the last committed offsets for all assigned partitions for the given topic. These offsets correspond to
     * the consumer position, i.e. the offsets of the next messages that should be polled/processed.
     *
     * If the last committed offsets are outside the range of valid offsets, or if offsets haven't yet been committed,
     * then the returns offset will be appropriately set/adjusted based on the 'auto.offset.reset' property defined
     * in the underlying client properties.
     *
     * @return The last committed offsets for all assigned partitions across all subscribed topics
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Map<Integer, Long> getLastCommittedOffsetsForAssignedPartitions(@Nonnull String topic) throws RuntimeException;

    /**
     * Returns the last committed offsets for all partitions (assigned or not) for the given topic. These offsets
     * correspond to the consumer position, i.e. the offsets of the next messages that should be polled/processed.
     *
     * @param topic Topic whose offsets to retrieve
     * @return The last committed offsets for all partitions for the given topic
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Map<Integer, Long> getLastCommittedOffsets(String topic) throws RuntimeException;

    /**
     * Retrieves the earliest offsets across all partitions (assigned or not) for the given topic.
     *
     * @param topic Topic whose offsets to retrieve
     * @return The earliest available offsets for the given topic
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Map<Integer, Long> getBeginningOffsets(String topic) throws RuntimeException;

    /**
     * Retrieves the latest offsets across all partitions (assigned or not) for the given topic.
     *
     * @param topic Topic whose offsets to retrieve
     * @return The latest available offsets for the given topic
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Map<Integer, Long> getEndOffsets(String topic) throws RuntimeException;

    /**
     * Returns a set of all partitions (assigned or not) for the given topic.
     *
     * @param topic Topic whose partitions to retrieve
     * @return A set of all partitions for the given topic
     * @throws RuntimeException If an error occurs while invoking the underlying consumer
     */
    @Nonnull
    Set<Integer> getPartitionsForTopic(String topic) throws RuntimeException;

    /**
     * Unsubscribes from topics and closes the underlying consumer
     */
    void shutdown();
}
