package com.pcb.kafkacore.consumer.model;

/**
 * Interface of the ConsumerRecord class used by the underlying KafkaConsumer API.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface ConsumerRecord<K, V> {

    String topic();

    int partition();

    K key();

    V value();

    long offset();

    long timestamp();

}
