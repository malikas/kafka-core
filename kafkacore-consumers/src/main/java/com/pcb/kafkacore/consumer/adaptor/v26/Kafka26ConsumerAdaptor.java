package com.pcb.kafkacore.consumer.adaptor.v26;

import com.pcb.kafkacore.consumer.adaptor.NativeConsumerAdaptor;
import com.pcb.kafkacore.consumer.model.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Adaptor class for the native Kafka 2.6 consumer client.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class Kafka26ConsumerAdaptor<K, V> implements NativeConsumerAdaptor<K, V> {

    private final KafkaConsumer<K, V> nativeConsumer;

    public  Kafka26ConsumerAdaptor(KafkaConsumer<K, V> nativeConsumer) {
        this.nativeConsumer = nativeConsumer;
    }

    @Override
    public void close() {
        nativeConsumer.close();
    }

    @Override
    public void subscribe(Collection<String> topics, com.pcb.kafkacore.consumer.model.ConsumerRebalanceListener listener) {
        nativeConsumer.subscribe(topics, new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> nativePartitions) {
                listener.onPartitionsRevoked(fromNativePartitions(nativePartitions));
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> nativePartitions) {
                listener.onPartitionsAssigned(fromNativePartitions(nativePartitions));
            }
        });
    }

    @Override
    public void assign(Collection<com.pcb.kafkacore.consumer.model.TopicPartition> partitions) {
        nativeConsumer.assign(toNativePartitions(partitions));
    }

    @Override
    public void unsubscribe() {
        nativeConsumer.unsubscribe();
    }

    @Override
    public Set<com.pcb.kafkacore.consumer.model.TopicPartition> assignment() {
        Set<TopicPartition> nativePartitions = nativeConsumer.assignment();
        return fromNativePartitions(nativePartitions);
    }

    @Override
    public List<Integer> partitionsFor(String topic) {
        List<PartitionInfo> nativePartitions = nativeConsumer.partitionsFor(topic);
        return nativePartitions == null ? null : nativePartitions.stream().map(PartitionInfo::partition).collect(Collectors.toList());
    }

    @Override
    public Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> beginningOffsets(Collection<com.pcb.kafkacore.consumer.model.TopicPartition> partitions) {
        Map<TopicPartition, Long> nativeOffsets = nativeConsumer.beginningOffsets(toNativePartitions(partitions));
        Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> results = null;
        if (nativeOffsets != null) {
            results = new HashMap<>();

            for (Map.Entry<TopicPartition, Long> e : nativeOffsets.entrySet()) {
                com.pcb.kafkacore.consumer.model.TopicPartition partition = fromNativePartition(e.getKey());
                Long offset = e.getValue();
                results.put(partition, offset);
            }
        }

        return results;
    }

    @Override
    public Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> endOffsets(Collection<com.pcb.kafkacore.consumer.model.TopicPartition> partitions) {
        Map<TopicPartition, Long> nativeOffsets = nativeConsumer.endOffsets(toNativePartitions(partitions));
        Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> results = null;
        if (nativeOffsets != null) {
            results = new HashMap<>();

            for (Map.Entry<TopicPartition, Long> e : nativeOffsets.entrySet()) {
                com.pcb.kafkacore.consumer.model.TopicPartition partition = fromNativePartition(e.getKey());
                Long offset = e.getValue();
                results.put(partition, offset);
            }
        }

        return results;
    }

    @Override
    public Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> committed(Set<com.pcb.kafkacore.consumer.model.TopicPartition> partitions) {
        Map<TopicPartition, OffsetAndMetadata> nativeResults = nativeConsumer.committed(toNativePartitions(partitions));
        Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> results = null;
        if (nativeResults != null) {
            results = new HashMap<>();

            for (Map.Entry<TopicPartition, OffsetAndMetadata> e : nativeResults.entrySet()) {
                com.pcb.kafkacore.consumer.model.TopicPartition partition = fromNativePartition(e.getKey());
                Long offset = e.getValue() == null ? null : e.getValue().offset();
                results.put(partition, offset);
            }
        }

        return results;
    }

    @Override
    public void seek(com.pcb.kafkacore.consumer.model.TopicPartition partition, long offset) {
        nativeConsumer.seek(toNativePartition(partition), offset);
    }

    @Override
    public ConsumerRecords<K, V> poll(long timeoutInMs) {
        org.apache.kafka.clients.consumer.ConsumerRecords<K, V> nativeRecords = nativeConsumer.poll(timeoutInMs);
        return Kafka26ConsumerRecordsAdaptor.from(nativeRecords);
    }

    @Override
    public void commitSync() {
        nativeConsumer.commitSync();
    }

    @Override
    public void commitSync(Map<com.pcb.kafkacore.consumer.model.TopicPartition, Long> offsets) {
        Map<TopicPartition, OffsetAndMetadata> nativeOffsets = null;

        if (offsets != null) {
            nativeOffsets = new HashMap<>();
            for (Map.Entry<com.pcb.kafkacore.consumer.model.TopicPartition, Long> e : offsets.entrySet()) {
                TopicPartition nativePartition = toNativePartition(e.getKey());
                OffsetAndMetadata nativeOffset = e.getValue() == null ? null : new OffsetAndMetadata(e.getValue());
                nativeOffsets.put(nativePartition, nativeOffset);
            }
        }

        nativeConsumer.commitSync(nativeOffsets);
    }

    private static TopicPartition toNativePartition(com.pcb.kafkacore.consumer.model.TopicPartition partition) {
        return partition == null ? null : new TopicPartition(partition.topic(), partition.partition());
    }

    private static Set<TopicPartition> toNativePartitions(Collection<com.pcb.kafkacore.consumer.model.TopicPartition> partitions) {
        return partitions == null ? null : partitions.stream().map(Kafka26ConsumerAdaptor::toNativePartition).collect(Collectors.toSet());
    }

    private static com.pcb.kafkacore.consumer.model.TopicPartition fromNativePartition(TopicPartition partition) {
        return partition == null ? null : new com.pcb.kafkacore.consumer.model.TopicPartition(partition.topic(), partition.partition());
    }

    private static Set<com.pcb.kafkacore.consumer.model.TopicPartition> fromNativePartitions(Collection<TopicPartition> partitions) {
        return partitions == null ? null : partitions.stream().map(Kafka26ConsumerAdaptor::fromNativePartition).collect(Collectors.toSet());
    }
}
