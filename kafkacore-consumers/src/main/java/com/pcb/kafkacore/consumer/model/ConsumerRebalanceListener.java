package com.pcb.kafkacore.consumer.model;

import java.util.Collection;

public interface ConsumerRebalanceListener {
    void onPartitionsRevoked(Collection<TopicPartition> revokedTopicTartitions);

    void onPartitionsAssigned(Collection<TopicPartition> assignedTopicPartitions);
}
